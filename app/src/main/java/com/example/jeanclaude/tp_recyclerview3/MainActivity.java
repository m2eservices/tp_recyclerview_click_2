package com.example.jeanclaude.tp_recyclerview3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnRecyclerViewItemClickListener {

    private final int categoryIcon[] = {
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher
    };

    private final String categoryName[] = {
            "Apple",
            "Samsung",
            "MI",
            "Motorola",
            "Nokia",
            "Oppo",
            "Micromax",
            "Honor",
            "Lenovo"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final RecyclerView mainRecyclerView = findViewById(R.id.activity_main_rv);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mainRecyclerView.setLayoutManager(linearLayoutManager);
        //Recycler Adapter
        final ArrayList<MainModel> mainModelArrayList = prepareList();
        final MainRecyclerAdapter mainRecyclerAdapter = new MainRecyclerAdapter(this, mainModelArrayList);
        mainRecyclerAdapter.setOnRecyclerViewItemClickListener(this);
        mainRecyclerView.setAdapter(mainRecyclerAdapter);
    }

    private ArrayList<MainModel> prepareList() {
        ArrayList<MainModel> mainModelList = new ArrayList<>();
        for (int i = 0; i < categoryName.length; i++) {
            MainModel mainModel = new MainModel();
            mainModel.setOfferName(categoryName[i]);
            mainModel.setOfferIcon(categoryIcon[i]);
            mainModelList.add(mainModel);
        }
        return mainModelList;
    }

    @Override
    public void onItemClick(int position, View view) {
        MainModel mainModel = (MainModel) view.getTag();
        switch (view.getId()) {
            case R.id.row_main_adapter_linear_layout:
                Toast.makeText(this, "Position clicked: " + String.valueOf(position) + ", " + mainModel.getOfferName(), Toast.LENGTH_LONG).show();
                break;
        }
    }

}